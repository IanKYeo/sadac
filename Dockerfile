#FROM python:3.5-alpine
#FROM frolvlad/alpine-glibc:alpine-3.8
FROM debian:latest


COPY requirements.txt /app/requirements.txt


ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN apt-get update --fix-missing && \
    apt-get update && apt-get install -y --no-install-recommends apt-utils && \
    apt-get install -y wget bzip2 ca-certificates curl git && \
    apt-get clean && \
    apt-get update && apt-get install -y unzip zip && \
    apt-get update && apt-get install -y dos2unix && \
    rm -rf /var/lib/apt/lists/*

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    /opt/conda/bin/conda clean -tipsy && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc

# Add PostgreSQL's repository. It contains the most recent stable release
#     of PostgreSQL, ``9.6``.
#RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list

# Update the Ubuntu and PostgreSQL repository indexes
#RUN apt-get update && apt-get install -y apt-utils apt-transport-https ca-certificates

# Install ``python-software-properties``, ``software-properties-common`` and PostgreSQL 9.6
#  There are some warnings (in red) that show up during the build. You can hide
#  them by prefixing each apt-get statement with DEBIAN_FRONTEND=noninteractive
#RUN apt-get -y -q install software-properties-common &&\
#		apt-get -y -q install --allow-unauthenticated postgresql-9.6 postgresql-client-9.6 postgresql-contrib-9.6

#RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list
#RUN apt-get update && apt-get install software-properties-common postgresql-9.3 

#RUN apt-get update && apt-get install postgresql-dev gcc python3-dev musl-dev 

##RUN conda install -c conda-forge -c dlr-sc -c pythonocc -c oce pythonocc-core==0.18.1 python=3.5 ## why has this stopped working??

#install the ifcopenshell-python package to python packages directory
ENV DEV /usr/local/lib/python3.5/dist-packages
ADD https://github.com/IfcOpenShell/IfcOpenShell/releases/download/v0.5.0-preview2/ifcopenshell-python27-master-9ad68db-linux64.zip $DEV.zip
##https://github.com/lorinma/eastbim-python/releases/download/test/ifcopenshell-python-2.7-0.5.0-preview1-linux64.zip $DEV.zip
RUN unzip $DEV.zip -d $DEV; rm $DEV.zip

WORKDIR /app
RUN pip install -r requirements.txt 
#RUN pip install gunicorn

COPY . /app
#RUN chmod +x boot.sh ./

RUN chmod +x boot.sh
WORKDIR /app/app
ENV FLASK_DEBUG 1
ENV DATABASE_URI postgresql+psycopg2://ian:d1llon@db:5432/postgres
ENV APP_SETTINGS config.Config

ENV FLASK_APP flask_main.py

RUN chmod +x manage.py

# Make entrypoint executable
RUN dos2unix docker-entrypoint.sh

#RUN python manage.py db upgrade

EXPOSE 5000
EXPOSE 5432

#ENTRYPOINT ["python3"]
CMD ["/bin/bash", "docker-entrypoint.sh"]

#CMD [ "gunicorn", "flask_main:app" ]