--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

-- Started on 2018-09-20 16:00:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12924)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2826 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 24622)
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: ian
--

CREATE TABLE public.alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE public.alembic_version OWNER TO ian;

--
-- TOC entry 198 (class 1259 OID 24629)
-- Name: organisation; Type: TABLE; Schema: public; Owner: ian
--

CREATE TABLE public.organisation (
    id integer NOT NULL,
    org_name character varying(128),
    "timestamp" timestamp without time zone
);


ALTER TABLE public.organisation OWNER TO ian;

--
-- TOC entry 197 (class 1259 OID 24627)
-- Name: organisation_id_seq; Type: SEQUENCE; Schema: public; Owner: ian
--

CREATE SEQUENCE public.organisation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organisation_id_seq OWNER TO ian;

--
-- TOC entry 2827 (class 0 OID 0)
-- Dependencies: 197
-- Name: organisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ian
--

ALTER SEQUENCE public.organisation_id_seq OWNED BY public.organisation.id;


--
-- TOC entry 200 (class 1259 OID 24639)
-- Name: users; Type: TABLE; Schema: public; Owner: ian
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(64),
    email character varying(64),
    password_hash character varying(128),
    is_admin boolean,
    org_id integer
);


ALTER TABLE public.users OWNER TO ian;

--
-- TOC entry 199 (class 1259 OID 24637)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: ian
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO ian;

--
-- TOC entry 2828 (class 0 OID 0)
-- Dependencies: 199
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ian
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2680 (class 2604 OID 24632)
-- Name: organisation id; Type: DEFAULT; Schema: public; Owner: ian
--

ALTER TABLE ONLY public.organisation ALTER COLUMN id SET DEFAULT nextval('public.organisation_id_seq'::regclass);


--
-- TOC entry 2681 (class 2604 OID 24642)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: ian
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2814 (class 0 OID 24622)
-- Dependencies: 196
-- Data for Name: alembic_version; Type: TABLE DATA; Schema: public; Owner: ian
--

COPY public.alembic_version (version_num) FROM stdin;
ca5904fdacf7
\.


--
-- TOC entry 2816 (class 0 OID 24629)
-- Dependencies: 198
-- Data for Name: organisation; Type: TABLE DATA; Schema: public; Owner: ian
--

COPY public.organisation (id, org_name, "timestamp") FROM stdin;
\.


--
-- TOC entry 2818 (class 0 OID 24639)
-- Dependencies: 200
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: ian
--

COPY public.users (id, username, email, password_hash, is_admin, org_id) FROM stdin;
1	ian1	ian1@bimsense.co.uk	pbkdf2:sha256:50000$LRl4lLeH$1028c5678b7af3275aed624f995a4e57c6369e35494cb86872fe6669e7f914e6	f	\N
2	ian2	ian2@bimsense.co.uk	pbkdf2:sha256:50000$Bowjsqiu$b91cf2baa9b892f06330aba2a3ff1e371d374f6472554610a5bf401e7197e2da	f	\N
\.


--
-- TOC entry 2829 (class 0 OID 0)
-- Dependencies: 197
-- Name: organisation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ian
--

SELECT pg_catalog.setval('public.organisation_id_seq', 1, false);


--
-- TOC entry 2830 (class 0 OID 0)
-- Dependencies: 199
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ian
--

SELECT pg_catalog.setval('public.users_id_seq', 2, true);


--
-- TOC entry 2683 (class 2606 OID 24626)
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: ian
--

ALTER TABLE ONLY public.alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- TOC entry 2687 (class 2606 OID 24634)
-- Name: organisation organisation_pkey; Type: CONSTRAINT; Schema: public; Owner: ian
--

ALTER TABLE ONLY public.organisation
    ADD CONSTRAINT organisation_pkey PRIMARY KEY (id);


--
-- TOC entry 2691 (class 2606 OID 24644)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: ian
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2684 (class 1259 OID 24635)
-- Name: ix_organisation_org_name; Type: INDEX; Schema: public; Owner: ian
--

CREATE INDEX ix_organisation_org_name ON public.organisation USING btree (org_name);


--
-- TOC entry 2685 (class 1259 OID 24636)
-- Name: ix_organisation_timestamp; Type: INDEX; Schema: public; Owner: ian
--

CREATE INDEX ix_organisation_timestamp ON public.organisation USING btree ("timestamp");


--
-- TOC entry 2688 (class 1259 OID 24650)
-- Name: ix_users_email; Type: INDEX; Schema: public; Owner: ian
--

CREATE UNIQUE INDEX ix_users_email ON public.users USING btree (email);


--
-- TOC entry 2689 (class 1259 OID 24651)
-- Name: ix_users_username; Type: INDEX; Schema: public; Owner: ian
--

CREATE UNIQUE INDEX ix_users_username ON public.users USING btree (username);


--
-- TOC entry 2692 (class 2606 OID 24645)
-- Name: users users_org_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: ian
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_org_id_fkey FOREIGN KEY (org_id) REFERENCES public.organisation(id);


-- Completed on 2018-09-20 16:01:00

--
-- PostgreSQL database dump complete
--

