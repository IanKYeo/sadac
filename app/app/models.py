# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 11:00:37 2018

@author: User
"""

from app import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from datetime import datetime
from hashlib import md5
from time import time
from app import login
from time import time
import jwt
from app import app


@login.user_loader
def load_user(id):
    return User.query.get(int(id))

'''userProject = db.Table('user_project', db.Column('user_id', db.Integer, db.ForeignKey('users.id'), primary_key=True),
    db.Column('project_id', db.Integer, db.ForeignKey('project.id'), primary_key=True))

modelStorey = db.Table('model_storey', db.Column('model_files_id', db.Integer, db.ForeignKey('model_files.id'), primary_key=True),
    db.Column('building_storey_id', db.Integer, db.ForeignKey('building_storey.id'), primary_key=True))'''



class User(UserMixin, db.Model):
    __tablename__= 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    is_admin = db.Column(db.Boolean, default=False)
    org_id = db.Column(db.Integer, db.ForeignKey('organisation.id'))


    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

class Organisation(db.Model):
    __tablename__='organisation'
    id = db.Column(db.Integer, primary_key=True)
    org_name = db.Column(db.String(128), index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
   

'''class Project(db.Model):
    __tablename__='project'
    id = db.Column(db.Integer, primary_key=True)
    project_name = db.Column(db.String(128), index=True)
    project_ref = db.Column(db.String(32), index=True)
    project_desc = db.Column(db.String(254), index=True)
    project_guid = db.Column(db.String(32), index=True)
    ref_elevation = db.Column(db.Integer, index=True)
    ref_latitude = db.Column(db.Integer, index=True)
    ref_longitude = db.Column(db.Integer, index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

class ModelFiles(db.Model):
    __tablename__= 'model_files'
    id = db.Column(db.Integer, primary_key=True)
    model_path = db.Column(db.String(254), index=True)
    model_name = db.Column(db.String(128), index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    current_model = db.Column(db.Boolean, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    spaceAreas = db.relationship('SpaceArea', backref='model', lazy='dynamic')
    classification = db.relationship('Classifications', backref='model', lazy='dynamic')
    ifc_object = db.relationship('Objects', backref='model', lazy='dynamic')

class BuildingStorey(db.Model):
    __tablename__='building_storey'
    id = db.Column(db.Integer, primary_key=True)
    storey_name = db.Column(db.String(128), index=True)
    storey_ref = db.Column(db.String(32), index=True)
    storey_desc = db.Column(db.String(254), index=True)
    elevation = db.Column(db.Integer, index=True)

class SpaceArea(db.Model):
    __tablename__= 'space_areas'
    id = db.Column(db.Integer, primary_key=True)
    room_number = db.Column(db.String(32), index=True)
    room_name = db.Column(db.String(64), index=True)
    floor_area = db.Column(db.Integer, index=True)
    bb_height = db.Column(db.Integer, index=True)
    room_perimeter = db.Column(db.Integer, index=True)
    room_classification = db.Column(db.String(64), index=True)
    guid = db.Column(db.String(32), index=True)
    #geometry = db.Column(db.Text, index=True)
    geom = db.Column(db.PickleType)
    building_storey_id = db.Column(db.Integer, db.ForeignKey('building_storey.id'))
    model_files_id = db.Column(db.Integer, db.ForeignKey('model_files.id'))
    propertyset = db.relationship('PropertySet', backref='space_area', lazy='dynamic')

class Classifications(db.Model):
    __tablename__='classifications'
    id = db.Column(db.Integer, primary_key=True)
    class_ref = db.Column(db.String(64), index=True)
    class_guid = db.Column(db.String(32), index=True)
    class_name = db.Column(db.String(254), index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    model_files_id = db.Column(db.Integer, db.ForeignKey('model_files.id'))
    class_system_id = db.Column(db.Integer, db.ForeignKey('class_system.id'))

class Objects(db.Model):
    __tablename__='objects'
    id = db.Column(db.Integer, primary_key=True)
    object_name = db.Column(db.String(64), index=True)
    object_desc = db.Column(db.String(128), index=True)
    object_guid = db.Column(db.String(32), index=True)
    #geom = db.Column(Geometry(geometry_type='POINT', srid=4326))
    #centre_of_mass = db.Column(Geometry(geometry_type='POINT', srid=4326))
    geom = db.Column(db.PickleType)
    centre_of_mass = db.Column(db.PickleType)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    model_files_id = db.Column(db.Integer, db.ForeignKey('model_files.id'))
    space_area_id = db.Column(db.Integer, db.ForeignKey('space_areas.id'))
    propertyset = db.relationship('PropertySet', backref='object', lazy='dynamic')

#classObject = db.Table('class_object', db.Column('classifications_id', db.Integer, db.ForeignKey('classifications.id'), primary_key=True),
#    db.Column('objects_id', db.Integer, db.ForeignKey('objects.id'), primary_key=True))

class ClassObject(db.Model):
    __tablename__='class_object'
    id = db.Column(db.Integer, primary_key=True)
    class_guid = db.Column(db.String(32), index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    objects_id = db.Column(db.Integer, db.ForeignKey('objects.id'))
    class_id = db.Column(db.Integer, db.ForeignKey('classifications.id'))

class ClassSpace(db.Model):
    __tablename__='class_space'
    id = db.Column(db.Integer, primary_key=True)
    class_guid = db.Column(db.String(32), index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    space_id = db.Column(db.Integer, db.ForeignKey('space_areas.id'))
    class_id = db.Column(db.Integer, db.ForeignKey('classifications.id'))

class PropertySet(db.Model):
    __tablename__='property_sets'
    id = db.Column(db.Integer, primary_key=True)
    property_set_name = db.Column(db.String(64), index=True)
    property_set_desc = db.Column(db.String(128), index=True)
    property_set_guid = db.Column(db.String(32), index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    model_files_id = db.Column(db.Integer, db.ForeignKey('model_files.id'))
    object_id = db.Column(db.Integer, db.ForeignKey('objects.id'))
    space_area_id = db.Column(db.Integer, db.ForeignKey('space_areas.id'))
    ind_property = db.relationship('Property', backref='property_sets', lazy='dynamic')

class Property(db.Model):
    __tablename__='property'
    id = db.Column(db.Integer, primary_key=True)
    property_name = db.Column(db.String(64), index=True)
    property_desc = db.Column(db.String(128), index=True)
    property_value = db.Column(db.String(128), index=True)
    property_type = db.Column(db.String(32), index=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    property_set_id = db.Column(db.Integer, db.ForeignKey('property_sets.id'))

class ClassSystem(db.Model):
    __tablename__='class_system'
    id = db.Column(db.Integer, primary_key=True)
    class_sys_name = db.Column(db.String(128), index=True)
    class_sys_ref = db.Column(db.String(64), index=True)
    class_sys_link = db.Column(db.String(256), index=True)'''








  